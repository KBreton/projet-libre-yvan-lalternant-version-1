<!DOCTYPE HTML>

<html class="no-js" lang="fr">

<head>
	<?php include('view/layout/headLayout.php'); ?>
	
	<link rel="stylesheet" href="public/css/footer_stylesheet.css"/>
	<link rel="stylesheet" href="public/css/profilepage_stylesheet.css"/>
	<title><?= getName(); ?> | Yvan l'alternant</title>
</head>

<body>
	<!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->
	
	<?php include('view/layout/navbarLayout.php'); ?>
	
	<!-- Body -->
	
	<div id="global_wrapper">
		<section id="peripheral_wrapper">
			<div class="profilesheet">
				<div class="username">
					<div class="avatar"><i class="fa fa-user-circle"></i></div>				
					<div class="name"><strong><?= getName(); ?></strong></div>
				</div>
				
				<div class="current_job"><p>Current Job</p></div>
				<div class="contacts"><span style="color:#68ACD1">0</span><br/>contacts</div>
				<div class="copyright">&copy; 2018 - Yvan l'alternant</div>
			</div>
		</section>
		
		<section id="main_wrapper">
			<a onclick="document.getElementById('biography').style.display='block'" class="infos bluebox background_transitions">Ma biographie</a>
			<a onclick="document.getElementById('educational_background').style.display='block'" class="infos pinkbox background_transitions">Mon parcours</a>
			<a onclick="document.getElementById('skills').style.display='block'" class="infos pinkbox background_transitions">Mes compétences</a>
			<a onclick="document.getElementById('personnal_info').style.display='block'" class="infos bluebox background_transitions">Mes informations</a>
		</section>
		
		<div id="biography" class="modal_box"> 
			<div class="modal_box_content box_animate_top box_card4">
				<header class="modal_box_content" id="box_color">
					<span onclick="document.getElementById('biography').style.display='none'" class="box_button box_display_topright">&times;</span>
						<h2 class="modal">Ma biographie</h2>
				</header>
				
				<div class="box_container">
					<p>Actuellement étudiant en formation de Développeur Informatique au CESI de Rouen, je suis à la recherche d'un contrat d'alternance.<br/>Passionné de Web, je souhaiterais, à terme, évoluer dans le développement Back-End en PHP.<br/>Pour plus d'informations, n'hésitez pas à me contacter via la messagerie !</p>
				</div>
				
				<footer class="box_container modal_footer" id="box_color"></footer>
			</div>
		</div>
		
		<div id="educational_background" class="modal_box"> 
			<div class="modal_box_content box_animate_top box_card4">
				<header class="modal_box_content" id="box_color">
					<span onclick="document.getElementById('educational_background').style.display='none'" class="box_button box_display_topright">&times;</span>
						<h2 class="modal">Mon parcours</h2>
				</header>
				
				<div class="box_container">
					<p class="background_timeline">2018 - 2020</p>
					<p class="studies">Développeur Informatique - <strong>CESI Alternance</strong>
					
					<p class="background_timeline">2017 - 2018</p>
					<p class="studies">Informatique - <strong>Université de Brest</strong>
					
					<p class="background_timeline">2015 - 2016</p>
					<p class="studies">Terminale ES - <strong>Lycée Félix le Dantec</strong>
				</div>
				
				<footer class="box_container modal_footer" id="box_color"></footer>
			</div>
		</div>
		
		<div id="skills" class="modal_box"> 
			<div class="modal_box_content box_animate_top box_card4">
				<header class="modal_box_content" id="box_color">
					<span onclick="document.getElementById('skills').style.display='none'" class="box_button box_display_topright">&times;</span>
						<h2 class="modal">Mes compétences</h2>
				</header>
				
				<div class="box_container">
					<p class="background_timeline">2018 - 2020</p>
					<p class="studies">Développeur Informatique - <strong>CESI Alternance</strong>
					
					<p class="background_timeline">2017 - 2018</p>
					<p class="studies">Informatique - <strong>Université de Brest</strong>
					
					<p class="background_timeline">2015 - 2016</p>
					<p class="studies">Terminale ES - <strong>Lycée Félix le Dantec</strong>
				</div>
				
				<footer class="box_container modal_footer" id="box_color"></footer>
			</div>
		</div>
	
		<div id="personnal_info" class="modal_box"> 
			<div class="modal_box_content box_animate_top box_card4">
				<header class="modal_box_content" id="box_color">
					<span onclick="document.getElementById('personnal_info').style.display='none'" class="box_button box_display_topright">&times;</span>
						<h2 class="modal">Mes informations</h2>
				</header>
				
				<div class="box_container">
					<p class="classinfo">
						<span style="font-weight:bold;margin-bottom:0.1em;color:#333333">Age</span> : 21 ans<br/>
						<span style="font-weight:bold;margin-top:0.1em;color:#333333">Permis</span> : Permis B<br/>
						<span style="font-weight:bold;margin-bottom:0.1em;color:#333333">Véhicule</span> : Oui<br/>
						<span style="font-weight:bold;margin-bottom:0.1em;color:#333333">Mobilité</span> : Rouen, Caen, Rennes, Nantes
					</p>
				</div>
				
				<footer class="box_container modal_footer" id="box_color"></footer>
			</div>
		</div>
	</div>
	
	<?php include('view/layout/footerLayout.php'); ?>
	
	<?php include('view/layout/javascriptsLayout.php'); ?>
</body>

</html>