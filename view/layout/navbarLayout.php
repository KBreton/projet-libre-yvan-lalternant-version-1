<!-- Navbar -->
	<nav>
		<div class="logo"><a href="index.php?action=home"><img src="public/img/logo.png" width="47px" height="50px" alt="Yvan l'alternant"/></a></div>
		
		<form action="#" method="post">
			<input type="search" name="q" placeholder="Rechercher.."/>
		</form>
		
		<ul class="actions">
			<li class="navbar_gateway background_transitions xlarge"><a href="index.php?action=home" class="navbar_link"><i class="fa fa-home"></i></a></li>
			<li class="navbar_gateway background_transitions xlarge"><a href="index.php?action=contact" class="navbar_link"><i class="fa fa-address-book"></i></a></li>
			<li class="navbar_gateway background_transitions xlarge"><a href="index.php?action=messaging" class="navbar_link"><i class="fa fa-envelope"></i></a></li>
			<li class="navbar_gateway background_transitions menu xlarge"><i class="fa fa-chevron-circle-down"></i></li>
		</ul>
	</nav>
	
	<div id="dropdown_menu">
		<ul class="dropdown">
			<li class="profile_gateway">
				<p class="username">
					<a href="index.php?action=profile&id=<?= $_SESSION['member_id'];?>" target="_blank" class="show_profile transitions"><?= getName(); ?></a>
				</p>
				
				<p class="job">Etudiant chez CESI Alternance</p>
			</li>
			<a href="index.php?action=logout" class="dropdown_actions"><li class="dropdown_gateway background_transitions">&Agrave; propos</li></a>
			<a href="index.php?action=logout" class="dropdown_actions"><li class="dropdown_gateway background_transitions">Mentions légales</li></a>
			<a href="index.php?action=logout" class="dropdown_actions"><li class="dropdown_gateway background_transitions">Déconnexion</li></a>
		</ul>
	</div>